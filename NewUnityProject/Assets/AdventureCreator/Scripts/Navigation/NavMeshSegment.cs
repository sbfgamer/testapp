﻿/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2014
 *	
 *	"NavMeshSegment.cs"
 * 
 *	This script is used for the NavMeshSegment prefab, which defines
 *	the area to be baked by the Unity Navigation window.
 * 
 */

using UnityEngine;
using System.Collections;
using AC;

public class NavMeshSegment : MonoBehaviour
{

	private void Awake ()
	{
		Hide ();

		#if !UNITY_5
		if (GetComponent <Collider>())
		{
			GetComponent <Collider>().isTrigger = true;
		}
		#endif

		if (KickStarter.sceneSettings)
		{
			if (KickStarter.sceneSettings.navigationMethod == AC_NavigationMethod.UnityNavigation)
			{
				if (LayerMask.NameToLayer (KickStarter.settingsManager.navMeshLayer) == -1)
				{
					Debug.LogWarning ("No 'NavMesh' layer exists - please define one in the Tags Manager.");
				}
				else
				{
					gameObject.layer = LayerMask.NameToLayer (KickStarter.settingsManager.navMeshLayer);
				}
			}
		}
	}
	
	
	public void Hide ()
	{
		if (this.GetComponent <MeshRenderer>())
		{
			this.GetComponent <MeshRenderer>().enabled = false;
		}
	}
	
	
	public void Show ()
	{
		if (this.GetComponent <MeshRenderer>() && this.GetComponent <MeshFilter>() && this.GetComponent <MeshCollider>() && this.GetComponent <MeshCollider>().sharedMesh)
		{
			this.GetComponent <MeshFilter>().mesh = this.GetComponent <MeshCollider>().sharedMesh;
			this.GetComponent <MeshRenderer>().enabled = true;
			this.GetComponent <MeshRenderer>().receiveShadows = false;
		}
	}
	
}
