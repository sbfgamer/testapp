/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2014
 *	
 *	"KickStarter.cs"
 * 
 *	This script will make sure that PersistentEngine and the Player gameObjects are always created,
 *	regardless of which scene the game is begun from.  It will also check the key gameObjects for
 *	essential scripts and references.
 * 
 */

using UnityEngine;
using System.Collections;

namespace AC
{

	public class KickStarter : MonoBehaviour
	{

		private static Player playerPrefab = null;
		private static MainCamera mainCameraPrefab = null;

		// Managers
		private static SceneManager sceneManagerPrefab = null;
		private static SettingsManager settingsManagerPrefab = null;
		private static ActionsManager actionsManagerPrefab = null;
		private static VariablesManager variablesManagerPrefab = null;
		private static InventoryManager inventoryManagerPrefab = null;
		private static SpeechManager speechManagerPrefab = null;
		private static CursorManager cursorManagerPrefab = null;
		private static MenuManager menuManagerPrefab = null;

		// PersistentEngine components
		private static Options optionsComponent = null;
		private static RuntimeInventory runtimeInventoryComponent = null;
		private static RuntimeVariables runtimeVariablesComponent = null;
		private static PlayerMenus playerMenusComponent = null;
		private static StateHandler stateHandlerComponent = null;
		private static SceneChanger sceneChangerComponent = null;
		private static SaveSystem saveSystemComponent = null;
		private static LevelStorage levelStorageComponent = null;

		// GameEngine components
		private static MenuSystem menuSystemComponent = null;
		private static Dialog dialogComponent = null;
		private static PlayerInput playerInputComponent = null;
		private static PlayerInteraction playerInteractionComponent = null;
		private static PlayerMovement playerMovementComponent = null;
		private static PlayerCursor playerCursorComponent = null;
		private static PlayerQTE playerQTEComponent = null;
		private static SceneSettings sceneSettingsComponent = null;
		private static NavigationManager navigationManagerComponent = null;
		private static ActionListManager actionListManagerComponent = null;
		private static LocalVariables localVariablesComponent = null;


		public static SceneManager sceneManager
		{
			get
			{
				if (sceneManagerPrefab != null) return sceneManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().sceneManager)
				{
					sceneManagerPrefab = AdvGame.GetReferences ().sceneManager;
					return sceneManagerPrefab;
				}
				return null;
			}
			set
			{
				sceneManagerPrefab = value;
			}
		}


		public static SettingsManager settingsManager
		{
			get
			{
				if (settingsManagerPrefab != null) return settingsManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().settingsManager)
				{
					settingsManagerPrefab = AdvGame.GetReferences ().settingsManager;
					return settingsManagerPrefab;
				}
				return null;
			}
			set
			{
				settingsManagerPrefab = value;
			}
		}


		public static ActionsManager actionsManager
		{
			get
			{
				if (actionsManagerPrefab != null) return actionsManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().actionsManager)
				{
					actionsManagerPrefab = AdvGame.GetReferences ().actionsManager;
					return actionsManagerPrefab;
				}
				return null;
			}
			set
			{
				actionsManagerPrefab = value;
			}
		}


		public static VariablesManager variablesManager
		{
			get
			{
				if (variablesManagerPrefab != null) return variablesManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().variablesManager)
				{
					variablesManagerPrefab = AdvGame.GetReferences ().variablesManager;
					return variablesManagerPrefab;
				}
				return null;
			}
			set
			{
				variablesManagerPrefab = value;
			}
		}


		public static InventoryManager inventoryManager
		{
			get
			{
				if (inventoryManagerPrefab != null) return inventoryManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().inventoryManager)
				{
					inventoryManagerPrefab = AdvGame.GetReferences ().inventoryManager;
					return inventoryManagerPrefab;
				}
				return null;
			}
			set
			{
				inventoryManagerPrefab = value;
			}
		}


		public static SpeechManager speechManager
		{
			get
			{
				if (speechManagerPrefab != null) return speechManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().speechManager)
				{
					speechManagerPrefab = AdvGame.GetReferences ().speechManager;
					return speechManagerPrefab;
				}
				return null;
			}
			set
			{
				speechManagerPrefab = value;
			}
		}


		public static CursorManager cursorManager
		{
			get
			{
				if (cursorManagerPrefab != null) return cursorManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().cursorManager)
				{
					cursorManagerPrefab = AdvGame.GetReferences ().cursorManager;
					return cursorManagerPrefab;
				}
				return null;
			}
			set
			{
				cursorManagerPrefab = value;
			}
		}


		public static MenuManager menuManager
		{
			get
			{
				if (menuManagerPrefab != null) return menuManagerPrefab;
				else if (AdvGame.GetReferences () && AdvGame.GetReferences ().menuManager)
				{
					menuManagerPrefab = AdvGame.GetReferences ().menuManager;
					return menuManagerPrefab;
				}
				return null;
			}
			set
			{
				menuManagerPrefab = value;
			}
		}


		public static Options options
		{
			get
			{
				if (optionsComponent != null) return optionsComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <Options>())
				{
					optionsComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <Options>();
					return optionsComponent;
				}
				return null;
			}
		}


		public static RuntimeInventory runtimeInventory
		{
			get
			{
				if (runtimeInventoryComponent != null) return runtimeInventoryComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeInventory>())
				{
					runtimeInventoryComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeInventory>();
					return runtimeInventoryComponent;
				}
				return null;
			}
		}


		public static RuntimeVariables runtimeVariables
		{
			get
			{
				if (runtimeVariablesComponent != null) return runtimeVariablesComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeVariables>())
				{
					runtimeVariablesComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeVariables>();
					return runtimeVariablesComponent;
				}
				return null;
			}
		}


		public static PlayerMenus playerMenus
		{
			get
			{
				if (playerMenusComponent != null) return playerMenusComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <PlayerMenus>())
				{
					playerMenusComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <PlayerMenus>();
					return playerMenusComponent;
				}
				return null;
			}
		}


		public static StateHandler stateHandler
		{
			get
			{
				if (stateHandlerComponent != null) return stateHandlerComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>())
				{
					stateHandlerComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>();
					return stateHandlerComponent;
				}
				return null;
			}
		}


		public static SceneChanger sceneChanger
		{
			get
			{
				if (sceneChangerComponent != null) return sceneChangerComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <SceneChanger>())
				{
					sceneChangerComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <SceneChanger>();
					return sceneChangerComponent;
				}
				return null;
			}
		}


		public static SaveSystem saveSystem
		{
			get
			{
				if (saveSystemComponent != null) return saveSystemComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <SaveSystem>())
				{
					saveSystemComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <SaveSystem>();
					return saveSystemComponent;
				}
				return null;
			}
		}


		public static LevelStorage levelStorage
		{
			get
			{
				if (levelStorageComponent != null) return levelStorageComponent;
				else if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <LevelStorage>())
				{
					levelStorageComponent = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <LevelStorage>();
					return levelStorageComponent;
				}
				return null;
			}
		}


		public static MenuSystem menuSystem
		{
			get
			{
				if (menuSystemComponent != null) return menuSystemComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <MenuSystem>())
				{
					menuSystemComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <MenuSystem>();
					return menuSystemComponent;
				}
				return null;
			}
		}


		public static Dialog dialog
		{
			get
			{
				if (dialogComponent != null) return dialogComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <Dialog>())
				{
					dialogComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <Dialog>();
					return dialogComponent;
				}
				return null;
			}
		}


		public static PlayerInput playerInput
		{
			get
			{
				if (playerInputComponent != null) return playerInputComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerInput>())
				{
					playerInputComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerInput>();
					return playerInputComponent;
				}
				return null;
			}
		}


		public static PlayerInteraction playerInteraction
		{
			get
			{
				if (playerInteractionComponent != null) return playerInteractionComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerInteraction>())
				{
					playerInteractionComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerInteraction>();
					return playerInteractionComponent;
				}
				return null;
			}
		}


		public static PlayerMovement playerMovement
		{
			get
			{
				if (playerMovementComponent != null) return playerMovementComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerMovement>())
				{
					playerMovementComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerMovement>();
					return playerMovementComponent;
				}
				return null;
			}
		}


		public static PlayerCursor playerCursor
		{
			get
			{
				if (playerCursorComponent != null) return playerCursorComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerCursor>())
				{
					playerCursorComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerCursor>();
					return playerCursorComponent;
				}
				return null;
			}
		}


		public static PlayerQTE playerQTE
		{
			get
			{
				if (playerQTEComponent != null) return playerQTEComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerQTE>())
				{
					playerQTEComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <PlayerQTE>();
					return playerQTEComponent;
				}
				return null;
			}
		}


		public static SceneSettings sceneSettings
		{
			get
			{
				if (sceneSettingsComponent != null) return sceneSettingsComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <SceneSettings>())
				{
					sceneSettingsComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <SceneSettings>();
					return sceneSettingsComponent;
				}
				return null;
			}
		}


		public static NavigationManager navigationManager
		{
			get
			{
				if (navigationManagerComponent != null) return navigationManagerComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <NavigationManager>())
				{
					navigationManagerComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <NavigationManager>();
					return navigationManagerComponent;
				}
				return null;
			}
		}


		public static ActionListManager actionListManager
		{
			get
			{
				if (actionListManagerComponent != null) return actionListManagerComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <ActionListManager>())
				{
					actionListManagerComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <ActionListManager>();
					return actionListManagerComponent;
				}
				return null;
			}
		}


		public static LocalVariables localVariables
		{
			get
			{
				if (localVariablesComponent != null) return localVariablesComponent;
				else if (GameObject.FindWithTag (Tags.gameEngine) && GameObject.FindWithTag (Tags.gameEngine).GetComponent <LocalVariables>())
				{
					localVariablesComponent = GameObject.FindWithTag (Tags.gameEngine).GetComponent <LocalVariables>();
					return localVariablesComponent;
				}
				return null;
			}
		}
	

		public static Player player
		{
			get
			{
				if (playerPrefab != null)
				{
					return playerPrefab;
				}
				else
				{
					if (GameObject.FindWithTag (Tags.player) && GameObject.FindWithTag (Tags.player).GetComponent <Player>())
					{
						playerPrefab = GameObject.FindWithTag (Tags.player).GetComponent <Player>();
						return playerPrefab;
					}
				}
				return null;
			}
		}


		public static MainCamera mainCamera
		{
			get
			{
				if (mainCameraPrefab != null)
				{
					return mainCameraPrefab;
				}
				else
				{
					if (GameObject.FindWithTag (Tags.mainCamera) && GameObject.FindWithTag (Tags.mainCamera).GetComponent <MainCamera>())
					{
						mainCameraPrefab = GameObject.FindWithTag (Tags.mainCamera).GetComponent <MainCamera>();
						return mainCameraPrefab;
					}
				}
				return null;
			}
		}


		public static void ResetPlayer (Player ref_player, int ID, bool resetReferences, Quaternion _rotation)
		{
			// Delete current player
			if (GameObject.FindWithTag (Tags.player))
			{
				DestroyImmediate (GameObject.FindWithTag (Tags.player));
			}

			// Load new player
			if (ref_player)
			{
				SettingsManager settingsManager = AdvGame.GetReferences ().settingsManager;

				Player newPlayer = (Player) Instantiate (ref_player, Vector3.zero, _rotation);
				newPlayer.ID = ID;
				newPlayer.name = ref_player.name;
				playerPrefab = newPlayer;
				DontDestroyOnLoad (newPlayer);

				if (GameObject.FindWithTag (Tags.persistentEngine))
				{
					GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeInventory>().SetNull ();
					GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeInventory>().RemoveRecipes ();

					// Clear inventory
					if (!settingsManager.shareInventory)
					{
						GameObject.FindWithTag (Tags.persistentEngine).GetComponent <RuntimeInventory>().localItems.Clear ();
					}

					SaveSystem saveSystem = GameObject.FindWithTag (Tags.persistentEngine).GetComponent <SaveSystem>();
					if (saveSystem.DoesPlayerDataExist (ID, false))
					{
						saveSystem.AssignPlayerData (ID, !settingsManager.shareInventory);
					}

					// Menus
					foreach (Menu menu in PlayerMenus.GetMenus ())
					{
						foreach (MenuElement element in menu.elements)
						{
							if (element is MenuInventoryBox)
							{
								MenuInventoryBox invBox = (MenuInventoryBox) element;
								invBox.ResetOffset ();
							}
						}
					}
				}

				if (newPlayer.GetComponent <Animation>())
				{
					// Hack: Force idle of Legacy characters
					AdvGame.PlayAnimClip (newPlayer.GetComponent <Animation>(), AdvGame.GetAnimLayerInt (AnimLayer.Base), newPlayer.idleAnim, AnimationBlendMode.Blend, WrapMode.Loop, 0f, null, false);
				}
				else if (newPlayer.spriteChild)
				{
					// Hack: update 2D sprites
					if (newPlayer.spriteChild.GetComponent <FollowSortingMap>())
					{
						newPlayer.spriteChild.GetComponent <FollowSortingMap>().UpdateSortingMap ();
					}
					newPlayer.UpdateSpriteChild (settingsManager.IsTopDown (), settingsManager.IsUnity2D ());
				}
				newPlayer.animEngine.PlayIdle ();
			}

			// Reset player references
			if (resetReferences)
			{
				KickStarter.sceneSettings.ResetPlayerReference ();
				KickStarter.playerMovement.AssignFPCamera ();
				KickStarter.stateHandler.IgnoreNavMeshCollisions ();
				_Camera[] cameras = FindObjectsOfType (typeof (_Camera)) as _Camera[];
				foreach (_Camera camera in cameras)
				{
					camera.ResetTarget ();
				}
			}
		}


		private void Awake ()
		{
			// Test for key imports
			References references = (References) Resources.Load (Resource.references);
			if (references)
			{
				SceneManager sceneManager = AdvGame.GetReferences ().sceneManager;
				SettingsManager settingsManager = AdvGame.GetReferences ().settingsManager;
				ActionsManager actionsManager = AdvGame.GetReferences ().actionsManager;
				InventoryManager inventoryManager = AdvGame.GetReferences ().inventoryManager;
				VariablesManager variablesManager = AdvGame.GetReferences ().variablesManager;
				SpeechManager speechManager = AdvGame.GetReferences ().speechManager;
				CursorManager cursorManager = AdvGame.GetReferences ().cursorManager;
				MenuManager menuManager = AdvGame.GetReferences ().menuManager;
				
				if (sceneManager == null)
				{
					Debug.LogError ("No Scene Manager found - please set one using the Adventure Creator Kit wizard");
				}
				
				if (settingsManager == null)
				{
					Debug.LogError ("No Settings Manager found - please set one using the Adventure Creator Kit wizard");
				}
				else
				{
					if (settingsManager.IsInLoadingScene ())
					{
						Debug.Log ("Bypassing regular AC startup because the current scene is the 'Loading' scene.");
						return;
					}
					if (!GameObject.FindGameObjectWithTag (Tags.player))
					{
						KickStarter.ResetPlayer (settingsManager.GetDefaultPlayer (), settingsManager.GetDefaultPlayerID (), false, Quaternion.identity);
					}
					else
					{
						KickStarter.playerPrefab = GameObject.FindWithTag (Tags.player).GetComponent <Player>();

						if (sceneChanger != null && sceneChanger.GetPlayerOnTransition () != null && settingsManager.playerSwitching == PlayerSwitching.DoNotAllow)
						{
							// Replace "prefab" player with a local one if one exists
							GameObject[] playerObs = GameObject.FindGameObjectsWithTag (Tags.player);
							foreach (GameObject playerOb in playerObs)
							{
								if (playerOb.GetComponent <Player>() && sceneChanger.GetPlayerOnTransition () != playerOb.GetComponent <Player>())
								{
									KickStarter.sceneChanger.DestroyOldPlayer ();
									KickStarter.playerPrefab = playerOb.GetComponent <Player>();
									break;
								}
						    }
					    }
					}

					/*if (LayerMask.NameToLayer (settingsManager.navMeshLayer) > -1)
					{
						if (LayerMask.NameToLayer (settingsManager.hotspotLayer) > -1)
						{
							Physics.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.hotspotLayer));
							Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.hotspotLayer));
						}

						if (LayerMask.NameToLayer (settingsManager.deactivatedLayer) > -1)
						{
							Physics.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.deactivatedLayer));
							Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.deactivatedLayer));
						}

						if (LayerMask.NameToLayer (settingsManager.backgroundImageLayer) > -1)
						{
							Physics.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.backgroundImageLayer));
							Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer(settingsManager.navMeshLayer), LayerMask.NameToLayer(settingsManager.backgroundImageLayer));
						}
					}*/
				}
				
				if (actionsManager == null)
				{
					Debug.LogError ("No Actions Manager found - please set one using the main Adventure Creator window");
				}
				
				if (inventoryManager == null)
				{
					Debug.LogError ("No Inventory Manager found - please set one using the main Adventure Creator window");
				}
				
				if (variablesManager == null)
				{
					Debug.LogError ("No Variables Manager found - please set one using the main Adventure Creator window");
				}
				
				if (speechManager == null)
				{
					Debug.LogError ("No Speech Manager found - please set one using the main Adventure Creator window");
				}

				if (cursorManager == null)
				{
					Debug.LogError ("No Cursor Manager found - please set one using the main Adventure Creator window");
				}

				if (menuManager == null)
				{
					Debug.LogError ("No Menu Manager found - please set one using the main Adventure Creator window");
				}
				
				if (GameObject.FindWithTag (Tags.player) == null && KickStarter.settingsManager.movementMethod != MovementMethod.None)
				{
					Debug.LogWarning ("No Player found - please set one using the Settings Manager, tagging it as Player and placing it in a Resources folder");
				}
				
			}
			else
			{
				Debug.LogError ("No References object found. Please set one using the main Adventure Creator window");
			}

			if (!GameObject.FindGameObjectWithTag (Tags.persistentEngine))
			{
				try
				{
					GameObject persistentEngine = (GameObject) Instantiate (Resources.Load (Resource.persistentEngine));
					persistentEngine.name = AdvGame.GetName (Resource.persistentEngine);
				}
				catch {}
			}

			if (GameObject.FindWithTag (Tags.persistentEngine) == null)
			{
				Debug.LogError ("No PersistentEngine prefab found - please place one in the Resources directory, and tag it as PersistentEngine");
			}
			else
			{
				GameObject persistentEngine = GameObject.FindWithTag (Tags.persistentEngine);
				
				if (persistentEngine.GetComponent <Options>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no Options component attached.");
				}
				if (persistentEngine.GetComponent <RuntimeInventory>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no RuntimeInventory component attached.");
				}
				if (persistentEngine.GetComponent <RuntimeVariables>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no RuntimeVariables component attached.");
				}
				if (persistentEngine.GetComponent <PlayerMenus>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no PlayerMenus component attached.");
				}
				if (persistentEngine.GetComponent <StateHandler>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no StateHandler component attached.");
				}
				if (persistentEngine.GetComponent <SceneChanger>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no SceneChanger component attached.");
				}
				if (persistentEngine.GetComponent <SaveSystem>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no SaveSystem component attached.");
				}
				if (persistentEngine.GetComponent <LevelStorage>() == null)
				{
					Debug.LogError (persistentEngine.name + " has no LevelStorage component attached.");
				}
			}
			
			if (GameObject.FindWithTag (Tags.mainCamera) == null)
			{
				Debug.LogError ("No MainCamera found - please click 'Organise room objects' in the Scene Manager to create one.");
			}
			else
			{
				if (GameObject.FindWithTag (Tags.mainCamera).GetComponent <MainCamera>() == null)
				{
					Debug.LogError ("MainCamera has no MainCamera component.");
				}
			}
			
			if (this.tag == Tags.gameEngine)
			{
				if (this.GetComponent <MenuSystem>() == null)
				{
					Debug.LogError (this.name + " has no MenuSystem component attached.");
				}
				if (this.GetComponent <Dialog>() == null)
				{
					Debug.LogError (this.name + " has no Dialog component attached.");
				}
				if (this.GetComponent <PlayerInput>() == null)
				{
					Debug.LogError (this.name + " has no PlayerInput component attached.");
				}
				if (this.GetComponent <PlayerInteraction>() == null)
				{
					Debug.LogError (this.name + " has no PlayerInteraction component attached.");
				}
				if (this.GetComponent <PlayerMovement>() == null)
				{
					Debug.LogError (this.name + " has no PlayerMovement component attached.");
				}
				if (this.GetComponent <PlayerCursor>() == null)
				{
					Debug.LogError (this.name + " has no PlayerCursor component attached.");
				}
				if (this.GetComponent <PlayerQTE>() == null)
				{
					Debug.LogError (this.name + " has no PlayerQTE component attached.");
				}
				if (this.GetComponent <SceneSettings>() == null)
				{
					Debug.LogError (this.name + " has no SceneSettings component attached.");
				}
				else
				{
					if (this.GetComponent <SceneSettings>().navigationMethod == AC_NavigationMethod.meshCollider && this.GetComponent <SceneSettings>().navMesh == null)
					{
						// No NavMesh, are there Characters in the scene?
						AC.Char[] allChars = GameObject.FindObjectsOfType (typeof(AC.Char)) as AC.Char[];
						if (allChars.Length > 0)
						{
							Debug.LogWarning ("No NavMesh set. Characters will not be able to PathFind until one is defined - please choose one using the Scene Manager.");
						}
					}
					
					if (this.GetComponent <SceneSettings>().defaultPlayerStart == null)
					{
						Debug.LogWarning ("No default PlayerStart set.  The game may not be able to begin if one is not defined - please choose one using the Scene Manager.");
					}
				}
				if (this.GetComponent <NavigationManager>() == null)
				{
					Debug.LogError (this.name + " has no NavigationManager component attached.");
				}
				if (this.GetComponent <ActionListManager>() == null)
				{
					Debug.LogError (this.name + " has no ActionListManager component attached.");
				}
			}
		}



		private void OnLevelWasLoaded ()
		{
			if (GameObject.FindWithTag (Tags.player) && GameObject.FindWithTag (Tags.player).GetComponent <Player>())
			{
				KickStarter.playerPrefab = GameObject.FindWithTag (Tags.player).GetComponent <Player>();
			}
		}


		public void TurnOnAC ()
		{
			if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>())
			{
				GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>().gameState = GameState.Normal;
			}
		}
		
		
		public void TurnOffAC ()
		{
			this.GetComponent <ActionListManager>().KillAllLists ();
			this.GetComponent <Dialog>().KillDialog (true, true);

			Moveable[] moveables = FindObjectsOfType (typeof (Moveable)) as Moveable[];
			foreach (Moveable moveable in moveables)
			{
				moveable.Kill ();
			}
			
			Char[] chars = FindObjectsOfType (typeof (Char)) as Char[];
			foreach (Char _char in chars)
			{
				_char.EndPath ();
			}

			if (GameObject.FindWithTag (Tags.persistentEngine) && GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>())
			{
				GameObject.FindWithTag (Tags.persistentEngine).GetComponent <StateHandler>().gameState = GameState.Cutscene;
			}
		}
		
	}

}