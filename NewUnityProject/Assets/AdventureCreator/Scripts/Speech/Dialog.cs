/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2014
 *	
 *	"Dialog.cs"
 * 
 *	This script handles the running of dialogue lines, speech or otherwise.
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace AC
{
	
	public class Dialog : MonoBehaviour
	{
		
		public bool isMessageAlive { get; set; }
		public bool foundAudio { get; set; }
		public bool continueFromSpeech = false;
		[HideInInspector] public bool isBackground = false;
		
		private bool currentLineHasAudio = false;
		
		private AC.Char speakerChar;
		private string speakerName;
		
		private bool isSkippable = false;
		private string displayText = "";
		private string fullText = "";
		private float displayDuration;
		private float endTime;
		
		[HideInInspector] public bool pauseGap = false;
		private int gapIndex = -1;
		private int continueIndex = -1	;
		private List<SpeechGap> speechGaps = new List<SpeechGap>();
		private string allGapText;
		private AudioSource defaultAudioSource;

		
		private void Awake ()
		{
			if (KickStarter.speechManager.textScrollSpeed == 0f)
			{
				Debug.LogError ("Cannot have a Text Scroll Speed of zero - please amend your Speech Manager");
			}
			
			if (KickStarter.sceneSettings.defaultSound && KickStarter.sceneSettings.defaultSound.GetComponent <AudioSource>())
			{
				defaultAudioSource = this.GetComponent <SceneSettings>().defaultSound.GetComponent <AudioSource>();
			}
		}
		
		
		public void UpdateSkipDialogue ()
		{
			if (isSkippable && isMessageAlive && KickStarter.playerInput != null)
			{
				if (pauseGap && !IsBackgroundSpeech ())
					//!isBackground)
				{
					if ((KickStarter.playerInput.mouseState == MouseState.SingleClick || KickStarter.playerInput.mouseState == MouseState.RightClick))
					{
						if (speechGaps[gapIndex].waitTime < 0f)
						{
							KickStarter.playerInput.mouseState = MouseState.Normal;
							pauseGap = false;
							if (KickStarter.speechManager.scrollSubtitles)
							{
								StartCoroutine ("EndMessage");
							}
						}
						else if (KickStarter.speechManager.allowSpeechSkipping)
						{
							KickStarter.playerInput.mouseState = MouseState.Normal;
							pauseGap = false;
						}
					}
				}
				
				else if (KickStarter.speechManager.displayForever)
				{
					if ((KickStarter.playerInput.mouseState == MouseState.SingleClick || KickStarter.playerInput.mouseState == MouseState.RightClick))
					{
						KickStarter.playerInput.mouseState = MouseState.Normal;
						
						if (KickStarter.stateHandler.gameState == GameState.Cutscene)
						{
							if (KickStarter.speechManager.endScrollBeforeSkip && KickStarter.speechManager.scrollSubtitles && displayText != fullText)
							{
								// Stop scrolling
								StopCoroutine ("StartMessage");
								displayText = fullText;
							}
							else
							{
								// Stop message
								isMessageAlive = false;
								StartCoroutine ("EndMessage");
							}
						}
					}
					
					else if (Time.time > endTime && isBackground)
					{
						// Stop message due to timeout
						StartCoroutine ("EndMessage");
					}
				}
				
				else if ((KickStarter.playerInput.mouseState == MouseState.SingleClick || KickStarter.playerInput.mouseState == MouseState.RightClick) && KickStarter.speechManager && KickStarter.speechManager.allowSpeechSkipping &&
				         (!IsBackgroundSpeech () || KickStarter.speechManager.allowGameplaySpeechSkipping))
				{
					KickStarter.playerInput.mouseState = MouseState.Normal;
					
					if (KickStarter.stateHandler.gameState == GameState.Cutscene || (KickStarter.speechManager.allowGameplaySpeechSkipping && KickStarter.stateHandler.gameState == GameState.Normal))
					{
						if (KickStarter.speechManager.endScrollBeforeSkip && KickStarter.speechManager.scrollSubtitles && displayText != fullText)
						{
							// Stop scrolling
							if (speechGaps.Count > 0 && speechGaps.Count > gapIndex)
							{
								while (gapIndex < speechGaps.Count && speechGaps[gapIndex].waitTime >= 0)
								{
									// Find next wait
									gapIndex ++;
								}
								
								if (gapIndex == speechGaps.Count)
								{
									StopCoroutine ("StartMessage");
									displayText = fullText;
								}
								else
								{
									pauseGap = true;
									displayText = fullText.Substring (0, speechGaps[gapIndex].characterIndex);
								}
							}
							else
							{
								StopCoroutine ("StartMessage");
								displayText = fullText;
							}
						}
						else
						{
							// Stop message
							isMessageAlive = false;
							StartCoroutine ("EndMessage");
						}
					}
				}
				
				else if (Time.time > endTime)
				{
					// Stop message due to timeout
					StartCoroutine ("EndMessage");
				}
			}
		}
		
		
		public string GetSpeaker ()
		{
			if (speakerChar)
			{
				if (speakerChar.speechLabel != "")
				{
					return speakerChar.speechLabel;
				}
				return speakerChar.name;
			}
			
			return "";
		}
		
		
		public AC.Char GetSpeakingCharacter ()
		{
			return speakerChar;
		}
		
		
		public Sprite GetPortraitSprite ()
		{
			if (speakerChar && speakerChar.portraitIcon.texture)
			{
				if (IsAnimating ())
				{
					if (speakerChar.isLipSyncing)
					{
						return speakerChar.portraitIcon.GetAnimatedSprite (speakerChar.GetLipSyncFrame ());
					}
					else
					{
						return speakerChar.portraitIcon.GetAnimatedSprite (true);
					}
				}
				else
				{
					return speakerChar.portraitIcon.GetSprite ();
				}
			}
			return null;
		}
		
		
		public Texture2D GetPortrait ()
		{
			if (speakerChar && speakerChar.portraitIcon.texture)
			{
				return speakerChar.portraitIcon.texture;
			}
			return null;
		}
		
		
		public bool IsAnimating ()
		{
			if (speakerChar && speakerChar.portraitIcon.isAnimated)
			{
				return true;
			}
			return false;
		}
		
		
		public Rect GetAnimatedRect ()
		{
			if (speakerChar != null && speakerChar.portraitIcon != null)
			{
				if (speakerChar.isLipSyncing)
				{
					return speakerChar.portraitIcon.GetAnimatedRect (speakerChar.GetLipSyncFrame ());
				}
				else if (speakerChar.isTalking)
				{
					return speakerChar.portraitIcon.GetAnimatedRect ();
				}
				else
				{
					return speakerChar.portraitIcon.GetAnimatedRect (0);
				}
			}
			return new Rect (0,0,0,0);
		}
		
		
		public Color GetColour ()
		{
			if (speakerChar)
			{
				return speakerChar.speechColor;
			}
			
			return Color.white;
		}
		
		
		public string GetLine ()
		{
			if (KickStarter.speechManager.keepTextInBuffer)
			{
				return displayText;
			}
			if (isMessageAlive && isSkippable)
			{
				return displayText;
			}
			return "";
		}
		
		
		public string GetFullLine ()
		{
			if (isMessageAlive && isSkippable)
			{
				return fullText;
			}
			return "";
		}
		
		
		private IEnumerator EndMessage ()
		{
			StopCoroutine ("StartMessage");
			isSkippable = false;
			
			if (speakerChar)
			{
				speakerChar.StopSpeaking ();
			}
			
			if (gapIndex >= 0 && gapIndex < speechGaps.Count)
			{
				gapIndex ++;
				StartCoroutine ("StartMessage", allGapText);
			}
			else
			{
				// Wait a short moment for fade-out
				yield return new WaitForSeconds (0.1f);
				isMessageAlive = false;
			}
		}
		
		
		private IEnumerator StartMessage (string message)
		{
			isMessageAlive = true;
			isSkippable = true;
			
			displayText = "";
			//message = AdvGame.ConvertTokens (message);
			fullText = message;
			pauseGap = false;
			
			endTime = Time.time + displayDuration;
			
			if (KickStarter.speechManager.scrollSubtitles)
			{
				// Start scroll the message
				float amount = 0f;
				
				while (amount < 1f)
				{
					if (!pauseGap)
					{
						amount += KickStarter.speechManager.textScrollSpeed / 100f / message.Length;
						if (amount > 1f)
						{
							amount = 1f;
						}
						
						int currentCharIndex = (int) (amount * message.Length);
						
						if (gapIndex > 0)
						{
							currentCharIndex += speechGaps[gapIndex-1].characterIndex;
							if (currentCharIndex > message.Length)
							{
								currentCharIndex = message.Length;
							}
						}
						
						string newText = message.Substring (0, currentCharIndex);
						
						if (displayText != newText && KickStarter.speechManager.textScrollCLip && !currentLineHasAudio)
						{
							if (defaultAudioSource)
							{
								if (!defaultAudioSource.isPlaying)
								{
									defaultAudioSource.PlayOneShot (KickStarter.speechManager.textScrollCLip);
								}
							}
							else
							{
								Debug.LogWarning ("Cannot play text scroll audio clip as no 'Default' sound prefab has been defined in the Scene Manager");
							}
						}
						
						displayText = newText;
						
						if (gapIndex >= 0 && speechGaps.Count > gapIndex)
						{
							if (currentCharIndex == speechGaps [gapIndex].characterIndex)
							{
								float waitTime = (float) speechGaps [gapIndex].waitTime;
								pauseGap = true;
								if (waitTime >= 0f)
								{
									float pauseEndTime = Time.time + waitTime;
									while (pauseGap && Time.time < pauseEndTime)
									{
										yield return new WaitForFixedUpdate ();
									}
								}
								else
								{
									while (pauseGap)
									{
										yield return new WaitForFixedUpdate ();
									}
								}
								pauseGap = false;
								gapIndex ++;
								amount = 0f;
							}
						}

						if (continueIndex >= 0)
						{
							if (currentCharIndex >= continueIndex)
							{
								continueIndex = -1;
								DoContinue ();
							}
						}
					}
					
					yield return new WaitForFixedUpdate ();
				}
				displayText = message;
			}
			else
			{
				if (gapIndex >= 0 && speechGaps.Count >= gapIndex)
				{
					if (gapIndex == speechGaps.Count)
					{
						displayText = message;
						foreach (SpeechGap gap in speechGaps)
						{
							endTime -= gap.waitTime;
						}
					}
					else
					{
						float waitTime = (float) speechGaps[gapIndex].waitTime;
						displayText = message.Substring (0, speechGaps[gapIndex].characterIndex);
						
						if (waitTime >= 0)
						{
							endTime = Time.time + waitTime;
						}
						else
						{
							pauseGap = true;
						}
					}
				}
				else
				{
					displayText = message;
				}

				if (continueIndex >= 0)
				{
					yield return new WaitForSeconds (continueIndex / KickStarter.speechManager.textScrollSpeed);
					DoContinue ();
					yield return new WaitForSeconds ((message.Length - continueIndex) / KickStarter.speechManager.textScrollSpeed);
				}
				else
				{
					yield return new WaitForSeconds (message.Length / KickStarter.speechManager.textScrollSpeed);
				}
			}
			
			if (endTime == Time.time)
			{
				endTime += 2f;
			}
		}
		
		
		public float StartDialog (AC.Char _speakerChar, string message, int lineNumber, string language, bool _isBackground, bool _noAnimation)
		{
			isMessageAlive = false;
			continueFromSpeech = false;
			currentLineHasAudio = false;
			isBackground = _isBackground;
			
			if (_speakerChar)
			{
				speakerChar = _speakerChar;
				speakerChar.isTalking = !_noAnimation;
				
				speakerName = _speakerChar.name;
				if (_speakerChar.GetComponent <Player>())
				{
					if (KickStarter.settingsManager.playerSwitching == PlayerSwitching.Allow || !KickStarter.speechManager.usePlayerRealName)
					{
						speakerName = "Player";
					}
				}
				
				if (_speakerChar.GetComponent <Hotspot>())
				{
					if (_speakerChar.GetComponent <Hotspot>().hotspotName != "")
					{
						speakerName = _speakerChar.GetComponent <Hotspot>().hotspotName;
					}
				}
				
				if (_speakerChar.portraitIcon != null)
				{
					_speakerChar.portraitIcon.Reset ();
				}
				
				if (!_noAnimation)
				{
					if (KickStarter.speechManager.lipSyncMode == LipSyncMode.Off)
					{
						speakerChar.isLipSyncing = false;
					}
					else if (KickStarter.speechManager.lipSyncMode == LipSyncMode.FromSpeechText || KickStarter.speechManager.lipSyncMode == LipSyncMode.ReadPamelaFile || KickStarter.speechManager.lipSyncMode == LipSyncMode.ReadSapiFile || KickStarter.speechManager.lipSyncMode == LipSyncMode.ReadPapagayoFile)
					{
						speakerChar.StartLipSync (GenerateLipSyncShapes (KickStarter.speechManager.lipSyncMode, lineNumber, speakerName, language, message));
					}
				}
			}
			else
			{
				if (speakerChar)
				{
					speakerChar.isTalking = false;
				}
				speakerChar = null;			
				speakerName = "Narrator";
			}
			
			message = AdvGame.ConvertTokens (message);
			
			// Play sound and time displayDuration to it
			if (lineNumber > -1 && speakerName != "" && KickStarter.speechManager.searchAudioFiles)
			{
				string filename = "Speech/";
				if (language != "" && KickStarter.speechManager.translateAudio)
				{
					// Not in original language
					filename += language + "/";
				}
				filename += speakerName + lineNumber;
				
				foundAudio = false;
				AudioClip clipObj = Resources.Load(filename) as AudioClip;
				if (clipObj)
				{
					AudioSource audioSource = null;
					currentLineHasAudio = true;
					
					if (_speakerChar != null)
					{
						if (!_noAnimation)
						{
							if (KickStarter.speechManager.lipSyncMode == LipSyncMode.FaceFX)
							{
								FaceFXIntegration.Play (speakerChar, speakerName + lineNumber, clipObj);
							}
						}
						
						if (_speakerChar.GetComponent <AudioSource>())
						{
							_speakerChar.GetComponent <AudioSource>().volume = KickStarter.options.optionsData.speechVolume;
							audioSource = _speakerChar.GetComponent <AudioSource>();
						}
						else
						{
							Debug.LogWarning (_speakerChar.name + " has no audio source component!");
						}
					}
					else if (KickStarter.player && KickStarter.player.GetComponent <AudioSource>())
					{
						KickStarter.player.GetComponent <AudioSource>().volume = KickStarter.options.optionsData.speechVolume;
						audioSource = KickStarter.player.GetComponent <AudioSource>();
					}
					else if (defaultAudioSource != null)
					{
						audioSource = defaultAudioSource;
					}
					
					if (audioSource != null)
					{
						audioSource.clip = clipObj;
						audioSource.loop = false;
						audioSource.Play();
						
						foundAudio = true;
					}
					
					displayDuration = clipObj.length;
				}
				else
				{
					displayDuration = KickStarter.speechManager.screenTimeFactor * (float) message.Length;
					if (displayDuration < 0.5f)
					{
						displayDuration = 0.5f;
					}
					
					Debug.Log ("Cannot find audio file: " + filename);
				}
			}
			else
			{
				displayDuration = KickStarter.speechManager.screenTimeFactor * (float) message.Length;
				if (displayDuration < 0.5f)
				{
					displayDuration = 0.5f;
				}
			}
			
			message = DetermineGaps (message);
			
			if (speechGaps.Count > 0)
			{
				gapIndex = 0;
				allGapText = message;
				
				foreach (SpeechGap gap in speechGaps)
				{
					displayDuration += (float) gap.waitTime;
				}
			}
			else
			{
				gapIndex = -1;
			}
			
			StopCoroutine ("StartMessage");
			StartCoroutine ("StartMessage", message);
			
			return displayDuration;
		}
		
		
		public void KillDialog (bool stopCharacter, bool forceMenusOff)
		{
			isSkippable = false;
			isMessageAlive = false;
			
			StopCoroutine ("StartMessage");
			StopCoroutine ("EndMessage");
			
			if (speakerChar && stopCharacter)
			{
				speakerChar.StopSpeaking ();
			}
			
			if (forceMenusOff)
			{
				KickStarter.playerMenus.ForceOffSubtitles ();
			}
		}
		
		
		private void OnDestroy ()
		{
			speakerChar = null;
			defaultAudioSource = null;
		}
		
		
		private string DetermineGaps (string _text)
		{
			speechGaps.Clear ();
			continueIndex = -1;
			
			if (_text != null)
			{
				if (_text.Contains ("[wait"))
				{
					while (_text.Contains ("[wait:"))
					{
						int startIndex = _text.IndexOf ("[wait:");
						int endIndex = startIndex + 7; // should be 6?
						if (_text.Substring (endIndex + 1).Length > 0 && _text.Substring (endIndex + 1, 1) == "]")
						{
							// Double digits
							endIndex += 1;
						}
						string waitTimeText = _text.Substring (startIndex + 6, endIndex - startIndex - 6);
						speechGaps.Add (new SpeechGap (startIndex, IntParseFast (waitTimeText)));
						_text = _text.Substring (0, startIndex) + _text.Substring (endIndex + 1);
					}
					while (_text.Contains ("[wait]"))
					{
						int startIndex = _text.IndexOf ("[wait]");
						speechGaps.Add (new SpeechGap (startIndex, -1));
						_text = _text.Substring (0, startIndex) + _text.Substring (startIndex + 6);
					}
				}

				if (_text.Contains ("[continue]"))
				{
					continueIndex = _text.IndexOf ("[continue]");
					_text = _text.Replace ("[continue]", "");
				}
			}

			return _text;
		}
		
		
		private int IntParseFast (string value)
		{
			int result = 0;
			for (int i=0; i<value.Length; i++)
			{
				char letter = value[i];
				result = 10 * result + (int) char.GetNumericValue (letter);
			}
			return result;
		}
		
		
		public bool HasPausing ()
		{
			if (speechGaps.Count > 0)
			{
				return true;
			}
			return false;
		}


		private void DoContinue ()
		{
			continueFromSpeech = true;
		}
		
		
		public List<LipSyncShape> GenerateLipSyncShapes (LipSyncMode _lipSyncMode, int lineNumber, string speakerName, string language, string _message)
		{
			List<LipSyncShape> lipSyncShapes = new List<LipSyncShape>();
			lipSyncShapes.Add (new LipSyncShape (0, 0f, KickStarter.speechManager.lipSyncSpeed));
			TextAsset textFile = null;
			
			if (lineNumber > -1 && speakerName != "" && KickStarter.speechManager.searchAudioFiles)
			{
				string filename = "Lipsync/";
				if (language != "" && KickStarter.speechManager.translateAudio)
				{
					// Not in original language
					filename += language + "/";
				}
				filename += speakerName + lineNumber;
				textFile = Resources.Load (filename) as TextAsset;
			}
			
			if (_lipSyncMode == LipSyncMode.ReadPamelaFile && textFile != null)
			{
				string[] pamLines = textFile.text.Split('\n');
				bool foundSpeech = false;
				float fps = 24f;
				foreach (string pamLine in pamLines)
				{
					if (!foundSpeech)
					{
						if (pamLine.Contains ("framespersecond:"))
						{
							string[] pamLineArray = pamLine.Split(':');
							float.TryParse (pamLineArray[1], out fps);
						}
						else if (pamLine.Contains ("[Speech]"))
						{
							foundSpeech = true;
						}
					}
					else if (pamLine.Contains (":"))
					{
						string[] pamLineArray = pamLine.Split(':');
						
						float timeIndex = 0f;
						float.TryParse (pamLineArray[0], out timeIndex);
						string searchText = pamLineArray[1].ToLower ().Substring (0, pamLineArray[1].Length-1);
						
						bool found = false;
						foreach (string phoneme in KickStarter.speechManager.phonemes)
						{
							string[] shapesArray = phoneme.ToLower ().Split ("/"[0]);
							if (!found)
							{
								foreach (string shape in shapesArray)
								{
									if (shape == searchText)
									{
										int frame = KickStarter.speechManager.phonemes.IndexOf (phoneme);
										lipSyncShapes.Add (new LipSyncShape (frame, timeIndex, KickStarter.speechManager.lipSyncSpeed, fps));
										found = true;
									}
								}
							}
						}
						if (!found)
						{
							lipSyncShapes.Add (new LipSyncShape (0, timeIndex, KickStarter.speechManager.lipSyncSpeed, fps));
						}
					}
				}
			}
			else if (_lipSyncMode == LipSyncMode.ReadSapiFile && textFile != null)
			{
				string[] sapiLines = textFile.text.Split('\n');
				foreach (string sapiLine in sapiLines)
				{
					if (sapiLine.StartsWith ("phn "))
					{
						string[] sapiLineArray = sapiLine.Split(' ');
						
						float timeIndex = 0f;
						float.TryParse (sapiLineArray[1], out timeIndex);
						string searchText = sapiLineArray[4].ToLower ().Substring (0, sapiLineArray[4].Length-1);
						bool found = false;
						foreach (string phoneme in KickStarter.speechManager.phonemes)
						{
							string[] shapesArray = phoneme.ToLower ().Split ("/"[0]);
							if (!found)
							{
								foreach (string shape in shapesArray)
								{
									if (shape == searchText)
									{
										int frame = KickStarter.speechManager.phonemes.IndexOf (phoneme);
										lipSyncShapes.Add (new LipSyncShape (frame, timeIndex, KickStarter.speechManager.lipSyncSpeed, 60f));
										found = true;
									}
								}
							}
						}
						if (!found)
						{
							lipSyncShapes.Add (new LipSyncShape (0, timeIndex, KickStarter.speechManager.lipSyncSpeed, 60f));
						}
					}
				}
			}
			else if (_lipSyncMode == LipSyncMode.ReadPapagayoFile && textFile != null)
			{
				string[] papagoyoLines = textFile.text.Split('\n');
				foreach (string papagoyoLine in papagoyoLines)
				{
					if (papagoyoLine != "" && !papagoyoLine.Contains ("MohoSwitch"))
					{
						string[] papagoyoLineArray = papagoyoLine.Split(' ');
						
						float timeIndex = 0f;
						float.TryParse (papagoyoLineArray[0], out timeIndex);
						string searchText = papagoyoLineArray[1].ToLower ().Substring (0, papagoyoLineArray[1].Length);
						
						bool found = false;
						if (!searchText.Contains ("rest") && !searchText.Contains ("etc"))
						{
							foreach (string phoneme in KickStarter.speechManager.phonemes)
							{
								string[] shapesArray = phoneme.ToLower ().Split ("/"[0]);
								if (!found)
								{
									foreach (string shape in shapesArray)
									{
										if (shape == searchText)
										{
											int frame = KickStarter.speechManager.phonemes.IndexOf (phoneme);
											lipSyncShapes.Add (new LipSyncShape (frame, timeIndex, KickStarter.speechManager.lipSyncSpeed, 24f));
											
											found = true;
										}
									}
								}
							}
						}
						if (!found)
						{
							lipSyncShapes.Add (new LipSyncShape (0, timeIndex, KickStarter.speechManager.lipSyncSpeed, 240f));
						}
					}
				}
			}
			else if (_lipSyncMode == LipSyncMode.FromSpeechText)
			{
				for (int i=0; i<_message.Length; i++)
				{
					int maxSearch = Mathf.Min (5, _message.Length - i);
					for (int n=maxSearch; n>0; n--)
					{
						string searchText = _message.Substring (i, n);
						searchText = searchText.ToLower ();
						
						foreach (string phoneme in KickStarter.speechManager.phonemes)
						{
							string[] shapesArray = phoneme.ToLower ().Split ("/"[0]);
							foreach (string shape in shapesArray)
							{
								if (shape == searchText)
								{
									int frame = KickStarter.speechManager.phonemes.IndexOf (phoneme);
									lipSyncShapes.Add (new LipSyncShape (frame, (float) i, KickStarter.speechManager.lipSyncSpeed));
									i += n;
									n = Mathf.Min (5, _message.Length - i);
									break;
								}
							}
						}
						
					}
					lipSyncShapes.Add (new LipSyncShape (0, (float) i, KickStarter.speechManager.lipSyncSpeed));
				}
			}
			
			if (lipSyncShapes.Count > 1)
			{
				lipSyncShapes.Sort (delegate (LipSyncShape a, LipSyncShape b) {return a.timeIndex.CompareTo (b.timeIndex);});
			}
			
			return lipSyncShapes;
		}
		
		
		
		private bool IsBackgroundSpeech ()
		{
			if (KickStarter.stateHandler.gameState == GameState.Normal)
			{
				return true;
			}
			return false;
		}
		
	}
	
	
	public struct SpeechGap
	{
		
		public int characterIndex;
		public int waitTime;
		
		public SpeechGap (int _characterIndex, int _waitTime)
		{
			characterIndex = _characterIndex;
			waitTime = _waitTime;
		}
		
	}
	
	
	public struct LipSyncShape
	{
		
		public int frame;
		public float timeIndex;
		
		
		public LipSyncShape (int _frame, float _timeIndex, float speed, float fps)
		{
			// Pamela / Sapi
			frame = _frame;
			timeIndex = (_timeIndex / 15f / speed / fps) + Time.time;
		}
		
		
		public LipSyncShape (int _frame, float _timeIndex, float speed)
		{
			// Automatic
			frame = _frame;
			timeIndex = (_timeIndex / 15f / speed) + Time.time;
		}
		
	}
	
}