/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2014
 *	
 *	"RememberConversation.cs"
 * 
 *	This script is attached to conversation objects in the scene
 *	with DialogOption states we wish to save.
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AC
{

	public class RememberConversation : Remember
	{

		public override string SaveData ()
		{
			ConversationData conversationData = new ConversationData();
			conversationData.objectID = constantID;

			if (GetComponent <Conversation>())
			{
				conversationData.optionStates = GetComponent <Conversation>().GetOptionStates ();
				conversationData.optionLocks = GetComponent <Conversation>().GetOptionLocks ();
			}

			return Serializer.SaveScriptData <ConversationData> (conversationData);
		}


		public override void LoadData (string stringData)
		{
			ConversationData data = Serializer.LoadScriptData <ConversationData> (stringData);
			if (data == null) return;

			if (GetComponent <Conversation>())
			{
				GetComponent <Conversation>().SetOptionStates (data.optionStates);
				GetComponent <Conversation>().SetOptionLocks (data.optionLocks);
			}
		}

	}


	[System.Serializable]
	public class ConversationData : RememberData
	{
		public List<bool> optionStates = new List<bool>();
		public List<bool> optionLocks = new List<bool>();
		
		public ConversationData () { }
	}

}